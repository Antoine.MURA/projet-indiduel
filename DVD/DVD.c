#include <SDL2/SDL.h>
#include <stdio.h>

int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;

    SDL_Window *window1 = NULL;       
    SDL_Renderer* renderer1;


    /* Initialisation de la SDL  + gestion de l'échec possible */
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_DisplayMode current;
    if (SDL_GetCurrentDisplayMode(0, &current) != 0)
    {
        exit(EXIT_FAILURE);
    }
    printf("%d %d\n", current.h, current.w);

    int taille_x=200;
    int taille_y=200;


    int posX=0, posY=0;
    window1 = SDL_CreateWindow("", posX, posY, taille_x, taille_y, SDL_WINDOW_BORDERLESS);


    if (window1 == NULL)
    {
        SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    renderer1 = SDL_CreateRenderer(window1,-1,0);
    SDL_SetRenderDrawColor(renderer1, 255, 0, 0, 255);
    SDL_RenderClear(renderer1);
    SDL_RenderPresent(renderer1); 


    int ecartX=0,ecartY=0;
    for (int i = 0; i < 1000; i++)
    {
        if(posX>=(current.w-taille_x)){
            ecartX=-5;
            SDL_SetRenderDrawColor(renderer1, 0, 0, 255, 255);
            SDL_RenderClear(renderer1);
            SDL_RenderPresent(renderer1); 
        }
        if(posX<=0){
            ecartX=5;
            SDL_SetRenderDrawColor(renderer1, 255, 0, 0, 255);
            SDL_RenderClear(renderer1);
            SDL_RenderPresent(renderer1); 
        }
        if(posY>=(current.h-taille_y-50)){ 
            ecartY=-5;
            SDL_SetRenderDrawColor(renderer1, 0, 255, 0, 255);
            SDL_RenderClear(renderer1);
            SDL_RenderPresent(renderer1); 
        }
        if(posY<=0){
            ecartY=5;
            SDL_SetRenderDrawColor(renderer1, 255, 255, 0, 255);
            SDL_RenderClear(renderer1);
            SDL_RenderPresent(renderer1); 
        }
        posX+=ecartX;
        posY+=ecartY;
        SDL_SetWindowPosition(window1,posX,posY);
        SDL_Delay(5);
    }
    SDL_Quit(); // la SDL

    return 0;
}