#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>




SDL_Texture* load_texture_from_image(char * file_image_name, SDL_Renderer *renderer ){
    SDL_Surface *my_image = NULL;        
    SDL_Texture* my_texture = NULL;       

    my_image = IMG_Load(file_image_name);   


    my_texture = SDL_CreateTextureFromSurface(renderer, my_image); 
    SDL_FreeSurface(my_image);                                   

    return my_texture;
  }







void rectangle(int x, int y, int w, int h, SDL_Renderer* renderer, int r, int g, int b){
    SDL_Rect rectangle; 
    SDL_SetRenderDrawColor(renderer, r, g, b, 255);                                
    rectangle.x = x;                                            
    rectangle.y = y;                                                 
    rectangle.w = w;                                               
    rectangle.h = h;                                            
                                                
    SDL_RenderFillRect(renderer, &rectangle);   
    SDL_RenderPresent(renderer); 
}

void carre(int x, int y, int taille, SDL_Renderer* renderer, int r, int g, int b){
    SDL_Rect rectangle; 
    SDL_SetRenderDrawColor(renderer, r, g, b, 255);                                
    rectangle.x = x-(taille/2);                                            
    rectangle.y = y-(taille/2);                                                
    rectangle.w = taille;                                               
    rectangle.h = taille;                                            
                                                
    SDL_RenderFillRect(renderer, &rectangle);   
    SDL_RenderPresent(renderer); 
}

void Run_animation(SDL_Texture * texture, int nbImages){               
    int w,h;
    SDL_QueryTexture(texture, NULL, NULL, &w, &h);
    printf("w : %d h : %d\n",w,h);

}

int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;

    SDL_Window *window = NULL;       
    SDL_Renderer* renderer;

    /* Initialisation de la SDL  + gestion de l'échec possible */
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_DisplayMode current;
    if (SDL_GetCurrentDisplayMode(0, &current) != 0)
    {
        exit(EXIT_FAILURE);
    }
    int taille_x=800;
    int taille_y=600;
    
    

    window = SDL_CreateWindow("", 0, 0, taille_x, taille_y,SDL_WINDOW_RESIZABLE);

    if (window == NULL)
    {
        SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    
    renderer = SDL_CreateRenderer(window,-1,0);
    SDL_Texture * Samus = load_texture_from_image("Samus.png",renderer);
    SDL_Rect source = {0}, window_dimensions = {0}, destination = {0}, state = {0};                     

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    SDL_QueryTexture(Samus, NULL, NULL, &source.w, &source.h);
    
    int nb_images = 5;                     
    float zoom = 4;                       
    int offset_x = source.w / nb_images, offset_y = source.h;           
    
    
    state.x = 0 ;                         
    state.y = 0 ;          
    state.w = offset_x;                   
    state.h = offset_y;                 

    destination.w = offset_x * zoom;     
    destination.h = offset_y * zoom;      

    destination.y =                      
    (window_dimensions.h - destination.h) /2;

    int speed = 16;
    // for(int i=0;i<30;i++) {
    //     destination.x += 20;                   
    //     state.x += offset_x;              
    //     state.x %= source.w;           

    //     SDL_RenderClear(renderer);           
    //     SDL_RenderCopy(renderer, Samus, &state, &destination);  
    //     SDL_RenderPresent(renderer);

    //     SDL_Delay(100);              
    // }
    SDL_RenderClear(renderer);            

    Run_animation(Samus, 5);
    SDL_Delay(5000);
    SDL_Quit();

    return 0;
}


