#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>





void rectangle(int x, int y, int w, int h, SDL_Renderer* renderer, int r, int g, int b){
    SDL_Rect rectangle; 
    SDL_SetRenderDrawColor(renderer, r, g, b, 255);                                
    rectangle.x = x;                                            
    rectangle.y = y;                                                 
    rectangle.w = w;                                               
    rectangle.h = h;                                            
                                                
    SDL_RenderFillRect(renderer, &rectangle);   
    SDL_RenderPresent(renderer); 
}

void carre(int x, int y, int taille, SDL_Renderer* renderer, int r, int g, int b){
    SDL_Rect rectangle; 
    SDL_SetRenderDrawColor(renderer, r, g, b, 255);                                
    rectangle.x = x-(taille/2);                                            
    rectangle.y = y-(taille/2);                                                
    rectangle.w = taille;                                               
    rectangle.h = taille;                                            
                                                
    SDL_RenderFillRect(renderer, &rectangle);   
    SDL_RenderPresent(renderer); 
}


float Func_to_graphX(float Ain, float Bin, float  Bout, float  val){
    val=val-Ain;
    
    float intervalleIn=Bin-Ain;
    float intervalleOut=Bout;
    float ret=((val/intervalleIn)*intervalleOut);
    return ret;
}


float Func_to_graphY(float Ain, float Bin, float  Bout, float  val){
    val=val-Ain;
    
    float intervalleIn=Bin-Ain;
    float intervalleOut=Bout;
    float ret=Bout-((val/intervalleIn)*intervalleOut);
    return ret;
}

float fonction1(float x){
    return x*x;
}
float fonction2(float x){
    return x*x*x;
}

float fonction3(float x){
    return 2*x+5;
}
float fonction4(float x){
    return fabs(x);
}

int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;

    SDL_Window *window = NULL;       
    SDL_Renderer* renderer;


    /* Initialisation de la SDL  + gestion de l'échec possible */
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_DisplayMode current;
    if (SDL_GetCurrentDisplayMode(0, &current) != 0)
    {
        exit(EXIT_FAILURE);
    }
    int taille_x=200;
    int taille_y=600;

    float abscisse[2]={-4,4};
    float ordonnee[2]={-9,9};

    float pas=0.05;
    

    window = SDL_CreateWindow("", 0, 0, taille_x, taille_y,SDL_WINDOW_RESIZABLE);


    if (window == NULL)
    {
        SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    renderer = SDL_CreateRenderer(window,-1,0);
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer); 

    

    for(int i=abscisse[0];i<abscisse[1];i++){
        SDL_Delay(10);
        rectangle(Func_to_graphX(abscisse[0],abscisse[1],taille_x,i), 0, 2, taille_y, renderer, 192,192, 192);
    }
    for(int i=ordonnee[0];i<ordonnee[1];i++){
        SDL_Delay(10);
        rectangle(0, Func_to_graphY(ordonnee[0],ordonnee[1],taille_y,i), taille_x, 2, renderer, 192,192, 192);
    }

    SDL_Delay(10);
    rectangle(0, Func_to_graphY(ordonnee[0],ordonnee[1],taille_y,0), taille_x, 2, renderer, 0, 0, 0);
    SDL_Delay(10);
    rectangle(Func_to_graphX(abscisse[0],abscisse[1],taille_x,0), 0, 2, taille_y, renderer, 0, 0, 0);

    float (*pf)(float)=&fonction2;
    for(float i=abscisse[0];i<abscisse[1];i+=pas){
        SDL_Delay(10);
        carre(Func_to_graphX(abscisse[0],abscisse[1],taille_x,i),Func_to_graphY(ordonnee[0],ordonnee[1],taille_y,(*pf)(i)),7,renderer,255,0,255);
    }
    
    SDL_Delay(5000);
    SDL_Quit(); // la SDL

    return 0;
}


